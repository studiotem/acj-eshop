<?php
add_action( 'wp_enqueue_scripts', 'razzi_child_enqueue_scripts', 20 );
function razzi_child_enqueue_scripts() {
	wp_enqueue_style( 'razzi-child-style', get_stylesheet_uri() );
}


if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title' 	=> 'Feeds settings',
        'menu_title'	=> 'Feeds',
        'menu_slug' 	=> 'feeds-settings',
        'capability'	=> 'edit_posts',
        'icon_url' => 'dashicons-media-document',
        'redirect'		=> false
    ));

}