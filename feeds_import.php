<?php
set_time_limit(0);
ini_set('max_execution_time', '0');

$wp_load = './wp-load.php';

if (file_exists($wp_load)) {

    require_once($wp_load);
    require_once(ABSPATH . "wp-admin" . '/includes/image.php');
    require_once(ABSPATH . "wp-admin" . '/includes/file.php');
    require_once(ABSPATH . "wp-admin" . '/includes/media.php');
} else {

    die('no-load');

}

if (have_rows('feeds', 'option')):

    while (have_rows('feeds', 'option')) : the_row();

        $urlFeed = get_sub_field('url_to_feed');
        $importFunction = get_sub_field('import_function');

        $importFunction($urlFeed);

    endwhile;

endif;

function loadCustomXml($urlFeed)
{
    $array = json_decode(json_encode((array)simplexml_load_file($urlFeed)), true);

    return $array;
}

function functionKarcher($urlFeed)
{
    $i = 0;
    if ($urlFeed) {
        $productsList = loadCustomXml($urlFeed);

        if ($productsList) {
            if (array_key_exists('channel', $productsList)) {
                if (array_key_exists('item', $productsList['channel'])) {
                    foreach ($productsList['channel']['item'] as $key => $product) {
                        if ($i == 0) {

                        $code = $product['ean'];

                        $product_id = wc_get_product_id_by_sku($code);

                        $properties = $imagesArray = [];

                        $name = $product['produkt'];
                        $shortDescription = $product['popis_produktu'];
                        $price = $product['cena'];
                        $cats = $product['kategoria'];

                        if (!$product_id) {
                            if (array_key_exists('url_obrazku', $product)) {
                                $imageId = uploadFileCustom($product['url_obrazku']);
                                $imagesArray[] = $imageId;
                            }
                        }

                        echo $key . ' ' . $name . '<br>';

                        $product = new WC_Product();

                        if ($product_id) {

                            $product = new WC_Product($product_id);

                        }

                        if (!$product_id) {
                            if (!empty($imagesArray)) {
                                $y = 0;
                                foreach ($imagesArray as $image) {
                                    if ($y == 0) {
                                        $product->set_image_id($image);
                                    }
                                    $y++;
                                }
                                //$product->set_gallery_image_ids($images);
                            }
                        }

                        $product->set_name($name);
                        $product->set_sku($code);
                        $product->set_description($shortDescription);
                        $product->set_regular_price($price);


                        $product->save();

                        $post_id = $product->get_id();

                        if ($cats) {
                            $categories = explode(">", $cats);
                            $append = true;
                            $cat_id = null;
                            $taxonomy = 'product_cat';
                            $lastCreated = null;
                            $catArray = [];

                            foreach ($categories as $keyCat => $category) {
                                $catNew = trim($category);
                                $exists = get_term_by('name', $catNew, $taxonomy);

                                if ($exists == false) {

                                    if ($keyCat > 0) {
                                        $catCreated = wp_insert_term($catNew, $taxonomy, array(
                                            'parent' => $lastCreated
                                        ));
                                    } else {
                                        $catCreated = wp_insert_term($catNew, $taxonomy);
                                    }

                                    $cat_id = $catCreated['term_id'];
                                } else {
                                    $cat_id = $exists->term_id;
                                }
                                $catArray[] = $cat_id;
                                $lastCreated = $cat_id;
                            }

                            if ($cat_id) {
                                wp_set_post_terms($post_id, $catArray, $taxonomy, $append);
                            }
                        }
                    }
                        //$i++;
                    }


                }
            }

            return true;
        }
    }

    return false;
}

function functionKompresor($urlFeed)
{
    $i = 0;
    if ($urlFeed) {

        $productsList = loadCustomXml($urlFeed);

        if ($productsList) {
            if (array_key_exists('SHOPITEM', $productsList)) {

                foreach ($productsList['SHOPITEM'] as $key => $product) {

                    if ($i == 0) {
                        $code = $product['CODE'];

                        $product_id = wc_get_product_id_by_sku($code);


                        $properties = $imagesArray = [];

                        $name = $product['NAME'];
                        $shortDescription = $product['SHORT_DESCRIPTION'];
                        $description = $product['DESCRIPTION'];
                        $price = $product['PRICE'];
                        $images = $product['IMAGES'];
                        $textProperties = $product['TEXT_PROPERTIES'];
                        $infoProperties = $product['INFORMATION_PARAMETERS'];
                        $cats = $product['CATEGORIES'];

                        if (!$product_id) {
                            if (array_key_exists('IMAGE', $images)) {
                                if (is_array($images['IMAGE'])) {
                                    $imageId = uploadFileCustom($images['IMAGE'][0]);
                                } else {
                                    $imageId = uploadFileCustom($images['IMAGE']);
                                }
                                $imagesArray[] = $imageId;
                            }
                        }

                        $product = new WC_Product();

                        if ($product_id) {

                            $product = new WC_Product($product_id);

                        }

                        echo $key . ' ' . $name . '<br>';

                        $product->set_name($name);
                        $product->set_sku($code);
                        $product->set_short_description($shortDescription);
                        $product->set_description($description);
                        $product->set_regular_price($price);

                        if (!$product_id) {
                            if (!empty($imagesArray)) {
                                $y = 0;
                                foreach ($imagesArray as $image) {
                                    if ($y == 0) {
                                        $product->set_image_id($image);
                                    }
                                    $y++;
                                }
                                //$product->set_gallery_image_ids($images);
                            }
                        }

                        $product->save();
                        $post_id = $product->get_id();

                        if ($cats) {
                            $cat = $cats['CATEGORY'];
                            $categories = explode(">", $cat);
                            $append = true;
                            $cat_id = null;
                            $taxonomy = 'product_cat';
                            $lastCreated = null;
                            $catArray = [];

                            foreach ($categories as $keyCat => $category) {
                                $catNew = trim($category);
                                $exists = get_term_by('name', $catNew, $taxonomy);

                                if ($exists == false) {

                                    if ($keyCat > 0) {
                                        $catCreated = wp_insert_term($catNew, $taxonomy, array(
                                            'parent' => $lastCreated
                                        ));
                                    } else {
                                        $catCreated = wp_insert_term($catNew, $taxonomy);
                                    }

                                    $cat_id = $catCreated['term_id'];
                                } else {
                                    $cat_id = $exists->term_id;
                                }
                                $catArray[] = $cat_id;
                                $lastCreated = $cat_id;
                            }

                            if ($cat_id) {
                                wp_set_post_terms($post_id, $catArray, $taxonomy, $append);
                            }
                        }

                        if (!empty($textProperties)) {
                            if (array_key_exists('TEXT_PROPERTY', $textProperties)) {

                                delete_post_meta($post_id, 'vlasnosti_produktu');
                                foreach ($textProperties['TEXT_PROPERTY'] as $textProperty) {

                                    $properties = [
                                        'nazov'   => $textProperty['NAME'],
                                        'hodnota' => $textProperty['VALUE'],
                                    ];

                                    insert_that_very_special_row($post_id, 'vlasnosti_produktu', $properties);
                                }
                            }
                        }

                        if (!empty($infoProperties)) {
                            if (array_key_exists('INFORMATION_PARAMETER', $infoProperties)) {

                                delete_post_meta($post_id, 'doplnujuce_parametre');
                                foreach ($infoProperties['INFORMATION_PARAMETER'] as $textProperty) {

                                    $properties = [
                                        'nazov'   => $textProperty['NAME'],
                                        'hodnota' => $textProperty['VALUE'],
                                    ];

                                    insert_that_very_special_row($post_id, 'doplnujuce_parametre', $properties);
                                }
                            }
                        }

                    }
                    //$i++;
                }

                return true;

            }
        }
    }

    return false;
}

function uploadFileCustom($imageurl)
{
    $tmp = explode('/', getimagesize($imageurl)['mime']);
    $imagetype = end($tmp);
    $uniq_name = date('dmY') . '' . (int)microtime(true);
    $filename = $uniq_name . '.' . $imagetype;

    $uploaddir = wp_upload_dir();
    $uploadfile = $uploaddir['path'] . '/' . $filename;
    $contents = file_get_contents($imageurl);
    $savefile = fopen($uploadfile, 'w');
    fwrite($savefile, $contents);
    fclose($savefile);

    $wp_filetype = wp_check_filetype(basename($filename), null);

    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_title'     => $filename,
        'post_content'   => '',
        'post_status'    => 'inherit'
    );

    $attach_id = wp_insert_attachment($attachment, $uploadfile);
    $imagenew = get_post($attach_id);
    $fullsizepath = get_attached_file($imagenew->ID);
    $attach_data = wp_generate_attachment_metadata($attach_id, $fullsizepath);
    wp_update_attachment_metadata($attach_id, $attach_data);

    return $attach_id;
}

add_action('acf/save_post', 'insert_that_very_special_row');

function insert_that_very_special_row($post_id, $rowName, $attrs)
{

    $row_count = count(get_field($rowName, $post_id) ?: []);

    //add_sub_row( array($rowName, ++$row_count, 'images'), $attrs );

    add_row($rowName, $attrs, $post_id);

    return true;
}
